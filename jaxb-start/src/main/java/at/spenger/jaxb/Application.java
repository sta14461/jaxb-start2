package at.spenger.jaxb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import at.spenger.jaxb.model.Book;
import at.spenger.jaxb.model.Bookstore;
import at.spenger.jaxb.model.User;

import org.json.JSONException;
import org.json.JSONObject;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application implements CommandLineRunner {

	private static final String BOOKSTORE_XML = "./bookstore-jaxb.xml";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    private void schreiben() throws JAXBException {
    	ArrayList<Book> bookList = new ArrayList<Book>();

		// create books
		Book book1 = new Book();
		book1.setIsbn("978-0060554736");
		book1.setName("The Game");
		book1.setAuthor("Neil Strauss");
		book1.setPublisher("Harpercollins");
		bookList.add(book1);

		Book book2 = new Book();
		book2.setIsbn("978-3832180577");
		book2.setName("Feuchtgebiete");
		book2.setAuthor("Charlotte Roche");
		book2.setPublisher("Dumont Buchverlag");
		bookList.add(book2);

		// create bookstore, assigning book
		Bookstore bookstore = new Bookstore();
		bookstore.setName("Fraport Bookstore");
		bookstore.setLocation("Frankfurt Airport");
		bookstore.setBookList(bookList);

		// create JAXB context and instantiate marshaller
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		// Write to System.out
		m.marshal(bookstore, System.out);

		// Write to File
		m.marshal(bookstore, new File(BOOKSTORE_XML));

    }
    
    
    private void lesen() throws JAXBException, FileNotFoundException {
    	System.out.println("Output from our XML File: ");
		// create JAXB context and instantiate marshaller
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		Unmarshaller um = context.createUnmarshaller();
		Bookstore bookstore2 = (Bookstore) um.unmarshal(new FileReader(
				BOOKSTORE_XML));
		ArrayList<Book> list = bookstore2.getBooksList();
		list.forEach(b -> System.out.println("Book: " + b.getName() + " from "
					+ b.getAuthor()));
    }

	@Override
	public void run(String... arg0) throws Exception {
		//schreiben();
		//lesen();
		//jsonschreiben();
		//jsonlesen();
		userSpeichern();
	}
	
	private void jsonschreiben() throws IOException {
		JsonObject model = Json.createObjectBuilder()
				.add("firstName", "Duke")
				.add("lastName", "Java")
				.add("age", 18)
				.add("streetAddress", "100 Internet Dr")
				.add("city", "JavaTown")
				.add("state", "JA")
				.add("postalCode", "12345")
				.add("phoneNumbers", Json.createArrayBuilder()
						.add(Json.createObjectBuilder()
								.add("type", "mobile")
								.add("number", "111-111-1111"))
								.add(Json.createObjectBuilder()
										.add("type", "home")
										.add("number", "222-222-2222")))
										.build();

		FileWriter stWriter = new FileWriter("a.json");

		try (JsonWriter jsonWriter = Json.createWriter(stWriter)) {
			jsonWriter.writeObject(model);
		}
	}
	private void jsonlesen() throws IOException
	{
		try(FileReader fr=new FileReader("a.json");
				JsonReader rdr=(JsonReader) Json.createReader(fr)){
			JsonObject obj=rdr.readObject();
			JsonArray results=obj.getJsonArray("phoneNumbers");

			results.forEach(System.out::println);
		}
	}




	/*
	 * JSON URL LESEN
	 */
	public static JSONObject readJsonURL(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try 
		{
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} 
		finally 
		{
			is.close();
		}
	}
	private static String readAll(Reader rdr) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rdr.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
	private void userSpeichern() throws IOException, JSONException
	{
		System.out.println("Read Graph API of Facebook-User");
		JSONObject json = readJsonURL("http://graph.facebook.com/bernhard.gally");

		System.out.println("JSON Object:");
		System.out.println(json.toString());

		System.out.println("Saving User:\n");	    
		
		User user=new User();
		user.setId(json.getString("id"));
		user.setFirstname(json.getString("first_name"));
		user.setGender(json.getString("gender"));
		user.setLastname(json.getString("last_name"));
		user.setName(json.getString("name"));

		System.out.println("User-ID: "+user.getId()+"\n"
				+"First Name: "+user.getFirstname()+"\n"
				+"Last Name: "+user.getLastname()+"\n"
				+"Sex: "+user.getGender()+"\n"
				+"Full-Name: "+user.getName()+"\n");
		}
}
